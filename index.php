<?php

    // Reset todos
    $todos = [];

    if (file_exists('todo.json')) {

            
        $json = file_get_contents('todo.json');

        $todos = json_decode($json, true);

    }

    // echo '</pre>';
    //     var_dump($todos);
    // echo '</pre>';

    
    // exit;


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Todo List</title>
</head>
<body>
    
            <form action="new_todo.php" method='post'>
                <input type="text" name="todo_name" placeholder="Enter todo...">
                <button type="submit">Enviar</button>
                
            </form>
            <br>
       
            <?php foreach ($todos as $todoName => $todo): ?>

                <div style="margin-bottom: 20px">
                    <form style="display: inline-block;" action="change_status.php" method="post">
                        <input type="checkbox" <?= $todo['completed'] ? 'checked' : '' ?>>    
                        <input type="hidden" name="todo_name" value="<?php echo $todoName; ?>">
                    </form>
                    <?php echo $todoName ?>
                    <!-- De esta manera se indexa mediante GET -->
                    <!-- <a href="delete.php?todo_name=<?php //echo $todoName; ?>">Delete</a> -->

                    <form style="display: inline-block;" action="delete.php" method="post">
                        <input type="hidden" name="todo_name" value="<?php echo $todoName; ?>">
                        <button>Delete</button>
                    </form>
                </div>

            <?php endforeach; ?>    
                
    <script>

                const checkboxes = document.querySelectorAll('input[type=checkbox]');

                checkboxes.forEach(ch => {

                    ch.onclick = function () {

                        this.parentNode.submit();
                    };
                });

    </script>   

</body>
</html>