<?php

        // echo '<pre>';
        //     var_dump($_POST);
        // echo '</pre>';  
        
        
    $todoName = trim($_POST['todo_name']) ?? '';
    
    // $todoName = trim($_POST[$todoName]);



    if ($todoName) {

        if (file_exists('todo.json')) {

                $json = file_get_contents('todo.json');
                
                //Convertimos el json extraido en array para ser legible en php
                $jsonArr = json_decode($json, true);

        } else {

            $jsonArr = [];

        }
        

        //Adjuntamos '$todoName' al json
        $jsonArr[$todoName] = ['completed' => false];

        // echo '<pre>';
        //     var_dump($jsonArr);
        // echo '</pre>';    

        file_put_contents('todo.json', json_encode($jsonArr, JSON_PRETTY_PRINT));



    }


    header('Location: ./index.php');