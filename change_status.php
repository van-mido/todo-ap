<?php

    echo '</pre>';
        var_dump($_POST);
    echo '</pre>';

    // Todo name para el estado
    $todoName = $_POST['todo_name'];

    // Primero ubicamos el archivo y guardamos contenido en una variable
    $json = file_get_contents('todo.json');
    $jsonArr = json_decode($json, true);

    // Actualizamos el estado del todo
    $jsonArr[$todoName]['completed'] = !$jsonArr[$todoName]['completed'];

    file_put_contents('todo.json', json_encode($jsonArr, JSON_PRETTY_PRINT));

    header('Location: ./index.php');
